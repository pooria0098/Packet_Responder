import argparse
from ARP_Response import *
from DNS_Response import *
from ICMP_Response import *


def showMenu():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--type', help='Type : arp/icmp/dns')
    parser.add_argument('-i', '--interface', help='interface : ens33')
    parser.add_argument('-d', '--data', help='Data')
    return parser


def main():
    args = showMenu().parse_args()

    if args.type == 'arp':
        print("Starting ARP reply ... ")
        args = showMenu().parse_args()
        Arp_Answer(args.interface)

    elif args.type == 'icmp':
        print("Starting ICMP reply ...")
        icmp_type = int(args.data[:args.data.find(',')])
        icmp_code = int(args.data[args.data.find(',') + 1:])
        Icmp_Answer(icmp_type, icmp_code)

    elif args.type == 'dns':
        print("Starting DNS reply ... ")
        Dns_Answer()


if __name__ == "__main__":
    main()
