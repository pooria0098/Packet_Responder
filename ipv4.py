import struct


class IPv4:

    def __init__(self, raw_data):
        version_header_length = raw_data[0]
        self.version = version_header_length >> 4
        # The minimum value for this field is 5,which indicates a length of 5 × 32 bits = 160 bits = 20 bytes. As a
        # 4-bit field, the maximum value is 15, this means that the maximum size of the IPv4 header is 15 × 32 bits,
        # or 480 bits = 60 bytes.
        self.header_length = (version_header_length & 15) * 4  # 4 bytes = 32 bits
        self.type_of_service, self.total_length, self.Identification, self.flags, self.ttl, self.proto, self.checksum, src, target = struct.unpack('! 1x B H H H B B H 4s 4s', raw_data[:20])
        self.src = self.ipv4(src)  # self.ipv4(src)
        self.target = self.ipv4(target)  # self.ipv4(target)
        self.data = raw_data[self.header_length:]

    # Returns properly formatted IPv4 address
    def ipv4(self, addr):
        return '.'.join(map(str, addr))
