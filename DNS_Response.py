import socket
from struct import pack
from ipv4 import IPv4
from udp import UDP
from PackingTcp import ip, port


def Dns_Answer():
    try:
        conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
        s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.getprotobyname('icmp'))
        while True:
            raw_data, addr = conn.recvfrom(65535)
            ip_header = IPv4(raw_data[14:])
            if ip_header.proto == 17:
                if ip_header.target == ip():
                    if UDP(ip_header.data).dest_port == 53:
                        print("---------------------------")
                        print("Request from IP : " + ip_header.src)
                        print("Response sent")
                        packet = pack('!H H H H H H', 1104, 33029, 1, 0, 0, 0)
                        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        sock.bind(('', port()))
                        sock.settimeout(2)
                        sock.sendto(bytes(packet), (ip_header.src, UDP(ip_header.data).dest_port))
    except:
        print("\nShut Down")
