import socket
from struct import pack
from ethernet import Ethernet
from arp import ARP
from general import *
from PackingTcp import ip


class Arp_Packing:
    def __init__(self, dest, src, src_mac_addr, src_ip_addr, dst_mac_addr, dst_ip_addr):
        self.dest = dest
        self.src = src
        self.prototype = 0x0806
        self.hardware_type = 0x001
        self.protocol_type = 0x0800
        self.hardware_size = 0x0006
        self.protocol_size = 0x0004
        self.Opcode = 0x0002
        self.src_mac_addr = src_mac_addr
        self.src_ip_addr = src_ip_addr
        self.dst_mac_addr = dst_mac_addr
        self.dst_ip_addr = dst_ip_addr
        self.packet = pack('! 6s 6s H H H B B H 6s 4s 6s 4s',
                           self.dest, self.src, self.prototype, self.hardware_type, self.protocol_type,
                           self.hardware_size, self.protocol_size,
                           self.Opcode, self.src_mac_addr, self.src_ip_addr,
                           self.dst_mac_addr, self.dst_ip_addr)


def Arp_Answer(mac):
    try:
        conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
        conn.bind((mac, socket.SOCK_RAW))
        while True:
            raw_data, addr = conn.recvfrom(65535)
            ETH_Header = Ethernet(raw_data)
            if ETH_Header.proto == 0x0806:
                if ARP(ETH_Header.data).Opcode == 1:
                    print("Mac Address of Ip address " + socket.inet_ntoa(
                        ARP(ETH_Header.data).dst_ip_addr) + " is " + get_mac_addr(conn.getsockname()[4]))
                    packet = Arp_Packing(ARP(ETH_Header.data).src_mac_addr, conn.getsockname()[4],
                                         conn.getsockname()[4],
                                         socket.inet_aton(ip()),
                                         ARP(ETH_Header.data).src_mac_addr, ARP(ETH_Header.data).src_ip_addr)
                    conn.send(packet.packet)
    except:
        print("\nShut Down")
