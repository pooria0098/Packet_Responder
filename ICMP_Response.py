import socket
from struct import pack
from ipv4 import IPv4
from PackingTcp import ip, checksum
from icmp import ICMP
import random


class Icmp_Packing:
    def __init__(self, type, code, id):
        self.type = type
        self.code = code
        self.checksum = 0
        self.packet_id = id
        self.sequence = 1
        self.data = 'Reply Massage ...'
        self.packet = pack('!B B H H H', self.type, self.code, self.checksum, self.packet_id,
                           self.sequence)
        self.checksum = checksum(self.packet + bytes(self.data, 'utf8'))
        self.packet = pack('!B B H H H', self.type, self.code, self.checksum, self.packet_id,
                           self.sequence) + bytes(self.data, 'utf8')


def Icmp_Answer(type, code):
    try:
        conn = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
        s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.getprotobyname('icmp'))
        while True:
            raw_data, addr = conn.recvfrom(65535)
            ip_header = IPv4(raw_data[14:])
            if ip_header.proto == 1:
                if ip_header.target == ip():
                    print("-------------------")
                    print("New ICMP request received From IP : " + str(ip_header.checksum))
                    print("We sent ICMP Reply with type: {},code : {}".format(type, code))
                    packet_id = int((id(1) * random.random()) / 65535)
                    packet = Icmp_Packing(type, code, packet_id)
                    s.sendto(packet.packet, (ip_header.src, 1))
    except:
        print("\nShut Down")
