import socket
from struct import *


class Packet:
    def __init__(self, src_addr, dest_addr, src_port, dest_port, type):
        # IP Header
        self.version = 4
        self.header_length = 5
        self.version_header_length = (self.version << 4) + self.header_length
        self.type_of_service = 0
        self.total_length = 40
        self.Identification = 54321
        self.flags = 0
        self.ttl = 255
        self.proto = socket.IPPROTO_TCP
        self.checksum = 10
        self.src = socket.inet_aton(src_addr)  # ipv4(source_address)
        self.target = socket.inet_aton(dest_addr)  # ipv4(destination_address)
        self.IPv4_header = pack('! B B H H H B B H 4s 4s', self.version_header_length, self.type_of_service,
                                self.total_length,
                                self.Identification, self.flags, self.ttl, self.proto,
                                self.checksum, self.src, self.target)
        # TCP Header
        self.src_port = src_port
        self.dest_port = dest_port
        self.sequence = 0
        self.acknowledgment = 0
        self.offset_reserved = (5 << 4)
        self.flag_urg = 0
        self.flag_psh = 0
        self.flag_rst = 0

        self.Select_TypeOfScan(type)

        self.tcp_flags = (self.flag_urg << 5) + (self.flag_ack << 4) + (self.flag_psh << 3) + (self.flag_rst << 2) + (
                self.flag_syn << 1) + self.flag_fin
        self.windows_size = 1024
        self.checksum = 0
        self.urgent_pointer = 0
        self.TCP_header = pack('!H H L L H H H H', self.src_port, self.dest_port, self.sequence,
                               self.acknowledgment, self.offset_reserved + self.tcp_flags,
                               self.windows_size, self.checksum, self.urgent_pointer)
        # TCP Checksum
        self.placeholder = 0
        self.tcp_length = len(self.TCP_header)
        self.tmp = pack('!4s 4s B B H', self.src, self.target, self.placeholder, self.proto,
                        self.tcp_length)
        self.tmp = self.tmp + self.TCP_header
        self.checksum = checksum(self.tmp)
        # Reassemble TCP Header
        self.TCP_header = pack('!H H L L H H H H', self.src_port, self.dest_port, self.sequence,
                               self.acknowledgment, self.offset_reserved + self.tcp_flags,
                               self.windows_size, self.checksum, self.urgent_pointer)
        self.packet = self.IPv4_header + self.TCP_header

    def Select_TypeOfScan(self, type):
        self.flag_ack = 0
        self.flag_syn = 0
        self.flag_fin = 0
        if type == 0:
            self.flag_syn = 1
        if type == 1:
            self.flag_ack = 1
        if type == 2:
            self.flag_fin = 1


def ip():
    # AF_INET = IPv4
    # SOCK_DGRAM = UDP
    # ورودی تابع connect() یک زوج دوتایی مرتب است که آدرس IP و پورت مقصد را مشخص می‌کند.
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.4.4', 1))  # Ip and dest port
    return s.getsockname()[0]  # sock.getsockname()[0] is the IP


def port():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(('', 0))
    return s.getsockname()[1]  # sock.getsockname()[1] is the port


def checksum(msg):
    s = 0
    # loop taking 2 characters at a time
    for i in range(0, len(msg), 2):
        w = (msg[i] << 8) + (msg[i + 1])
        s = s + w

    s = (s >> 16) + (s & 0xffff)
    # s = s + (s >> 16);
    # complement and mask to 4 byte short
    s = ~s & 0xffff
    return s
