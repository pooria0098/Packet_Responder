import struct


class ARP:
    def __init__(self, raw_data):
        self.hardware_type, self.protocol_type, self.hardware_size, self.protocol_size, self.Opcode, \
        self.src_mac_addr, self.src_ip_addr, self.dst_mac_addr, self.dst_ip_addr = \
            struct.unpack("! H H B B H 6s 4s 6s 4s", raw_data[:28])

        self.src_mac_addr = self.mac(self.src_mac_addr)
        self.src_ip_addr = self.ip(self.src_ip_addr)
        self.dst_mac_addr = self.mac(self.dst_mac_addr)
        self.dst_ip_addr = self.ip(self.dst_ip_addr)
        self.data = raw_data[28:]

    def ip(self, addr):
        return '.'.join(map(str, addr))

    def mac(self, addr):
        return ':'.join(map(str, addr))
